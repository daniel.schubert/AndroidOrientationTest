package android.daniel.test.orientationtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button defaultDemoButton = (Button) findViewById(R.id.defaultDemo);
        defaultDemoButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), RotationVectorDemo.class);
                startActivity(i);
            }
        });

        Button mapDemoButton = (Button) findViewById(R.id.mapDemo);
        mapDemoButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), MapDemo.class);
                startActivity(i);
            }
        });

        Button skyViewDemoButton = (Button) findViewById(R.id.skyViewDemo);
        skyViewDemoButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), SkyViewDemo.class);
                startActivity(i);
            }
        });
    }
}
