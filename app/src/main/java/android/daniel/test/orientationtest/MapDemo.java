package android.daniel.test.orientationtest;


import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

/**
 * Wrapper activity demonstrating the use of the new
 * {@link SensorEvent#values rotation vector sensor}
 * ({@link Sensor#TYPE_ROTATION_VECTOR TYPE_ROTATION_VECTOR}).
 *
 * @see Sensor
 * @see SensorEvent
 * @see SensorManager
 *
 */
public class MapDemo extends Activity {
    private SensorManager mSensorManager;
    private DrawView view;

    private Paint bgPaint, linePaint, targetPaint;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get an instance of the SensorManager
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        view = new DrawView(this, null);
        // Create our Preview view and set it as the content of our
        // Activity
        setContentView(view);

        bgPaint = new Paint();
        bgPaint.setColor(Color.BLACK);
        bgPaint.setStyle(Paint.Style.FILL);

        linePaint = new Paint();
        linePaint.setColor(Color.LTGRAY);
        linePaint.setStyle(Paint.Style.STROKE);

        targetPaint = new Paint();
        targetPaint.setColor(Color.WHITE);
        targetPaint.setStyle(Paint.Style.STROKE);
        targetPaint.setStrokeWidth(2);
        targetPaint.setTextSize(30);
    }
    @Override
    protected void onResume() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onResume();
        view.start();
    }
    @Override
    protected void onPause() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onPause();
        view.stop();
    }

    class DrawView extends View implements SensorEventListener {
        private Sensor mRotationVectorSensor;
        private final float[] mRotationMatrix = new float[9];
        public DrawView(Context context, AttributeSet attrs) {
            super(context, attrs);
            // find the rotation-vector sensor
            mRotationVectorSensor = mSensorManager.getDefaultSensor(
                    Sensor.TYPE_ROTATION_VECTOR);
            // initialize the rotation matrix to identity
            mRotationMatrix[ 0] = 1;
            mRotationMatrix[ 4] = 1;
            mRotationMatrix[ 8] = 1;
        }
        public void start() {
            // enable our sensor when the activity is resumed, ask for
            // 10 ms updates.
            mSensorManager.registerListener(this, mRotationVectorSensor,
                    SensorManager.SENSOR_DELAY_GAME);
        }
        public void stop() {
            // make sure to turn our sensor off when the activity is paused
            mSensorManager.unregisterListener(this);
        }
        public void onSensorChanged(SensorEvent event) {
            // we received a sensor event. it is a good practice to check
            // that we received the proper event
            if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
                // convert the rotation-vector to a 4x4 matrix. the matrix
                // is interpreted by Open GL as the inverse of the
                // rotation-vector, which is what we want.
                SensorManager.getRotationMatrixFromVector(
                        mRotationMatrix , event.values);
                invalidate();
            }
        }
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            int w = this.getWidth(),
                    h = this.getHeight(),
                    mx = w / 2,
                    my = h / 2;
            double maxR = Math.min(mx, my) * 0.8;

            // Background
            canvas.drawRect(0, 0, w, h, bgPaint);

            // Sky Map (Circles, Lines)
            for(int i = 0; i < 4; i++){
                canvas.drawCircle(mx, my, (float)(maxR - (maxR/4 * i)), linePaint);
            }
            for(int i = 0; i < 8; i++){
                canvas.drawLine(mx, my,
                        (float)(mx + maxR*Math.sin(i*Math.PI/4)),
                        (float)(my + maxR*Math.cos(i*Math.PI/4)),
                        linePaint);
            }

            // Target position
            float calc_x = mRotationMatrix[2],
                    calc_y = mRotationMatrix[5];

            float tx = (float) (mx + calc_x * maxR),
                    ty = (float) (my + calc_y * maxR),
                    tr = 20;

            // Target
            canvas.drawCircle(tx, ty, tr, targetPaint);
            canvas.drawLine(tx-tr, ty, tx+tr, ty, targetPaint);
            canvas.drawLine(tx, ty-tr, tx, ty+tr, targetPaint);

            if(mRotationMatrix[5] < 0.4) {
                // Draw North-Symbol (only when pointing looking kinda north)
                canvas.drawText("N",
                        (float) (mx + mRotationMatrix[3] * (maxR + 5)),
                        (float) (my - mRotationMatrix[4] * (maxR + 5)),
                        targetPaint);
            }
            if(mRotationMatrix[5] > -0.4) {
                // Draw South-Symbol (only when looking kinda south
                canvas.drawText("S",
                        (float) (mx - mRotationMatrix[3] * (maxR + 5)),
                        (float) (my + mRotationMatrix[4] * (maxR + 5)),
                        targetPaint);
            }
        }
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }
}